# Group_24

# Flight Data Analyzer

Flight Data Analyzer is a comprehensive tool designed to analyze flight data efficiently. It utilizes the `FlightDataAnalyzer` class to process and interpret various aspects of flight data, making it an invaluable resource for researchers, data scientists, and enthusiasts interested in aviation analytics.

## Description

The project consists of a Python file containing the `FlightDataAnalyzer` class, which provides a suite of methods to analyze flight data. Additionally, a Jupyter notebook is included to showcase how these methods can be applied to real-world datasets to extract meaningful insights.

## Installation

To ensure a smooth setup and execution environment, we've included a YAML file that automates the environment setup with all the necessary packages. Follow these steps to prepare your environment:

```bash
# Clone the repository
git clone https://gitlab.com/oliveiraluis418/group_24.git
cd group_24

# Create and activate the environment
conda env create -f icaras_environment.yaml
conda activate flight_data_analyzer_env
```

## Usage

Once the environment is set up, you can start using the Flight Data Analyzer by running the showcase notebook. This notebook demonstrates how to use the FlightDataAnalyzer class to perform various analyses on flight data.

Follow the instructions in the notebook to learn how to utilize the class for your data analysis needs.

## Documentation

For more detailed information on the methods available in the FlightDataAnalyzer class and additional guidance on how to use them, refer to the documentation provided in the docs folder.

The file you are looking for is called "index.html". You can find the file by opening the _build folder and html folder.

## Contact

If you have any questions or would like to contribute to the project, please feel free to contact us.

Luis Oliveira (43123@novasbe.pt)

Gonçalo Lourenço (60329@novasbe.pt)

Miguel Brito (61226@novasbe.pt)

