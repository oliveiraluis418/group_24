import os
import pandas as pd
import requests
from pydantic import BaseModel
import matplotlib.pyplot as plt
import geopandas as gpd
from shapely.geometry import LineString
from math import radians, sin, cos, sqrt, atan2


class Airport(BaseModel):
    code: str
    latitude: float
    longitude: float


class FlightDataAnalyzer:
    def __init__(self, download_dir: str = "downloads/"):
        """
        Initializes the FlightDataAnalyzer class with specified download directory.

        :param download_dir: The directory where downloaded files will be saved, defaults to 'downloads/'
        :type download_dir: str, optional
        """
        # Define URLs within the __init__ method
        self.flights_data_url = (
            "https://gitlab.com/oliveiraluis418/group_24/-/raw/BRANCH_1"
            "/airlines.csv?ref_type=heads"
        )
        self.airports_data_url = (
            "https://gitlab.com/oliveiraluis418/group_24/-/raw/BRANCH_1"
            "/airports.csv?ref_type"
            "=heads"
        )
        self.airplanes_data_url = (
            "https://gitlab.com/oliveiraluis418/group_24/-/raw/BRANCH_1"
            "/airplanes.csv?ref_type"
            "=heads"
        )
        self.routes_data_url = (
            "https://gitlab.com/oliveiraluis418/group_24/-/raw/BRANCH_1/routes"
            ".csv?ref_type=heads"
        )

        self.download_dir = download_dir
        self.flights_data_path = self.download_data(self.flights_data_url)
        self.airports_data_path = self.download_data(self.airports_data_url)
        self.airplanes_data_path = self.download_data(self.airplanes_data_url)
        self.routes_data_path = self.download_data(self.routes_data_url)

        self.flights_df = pd.read_csv(self.flights_data_path)
        self.airports_df = pd.read_csv(self.airports_data_path)
        self.airplanes_df = pd.read_csv(self.airplanes_data_path)
        self.routes_df = pd.read_csv(self.routes_data_path)

    def download_data(self, data_url: str) -> str:
        """
        Downloads data from the specified URL and saves it in the download directory.

        :param data_url: The URL from which to download data
        :type data_url: str
        :return: The filepath where the data is saved
        :rtype: str
        """
        os.makedirs(self.download_dir, exist_ok=True)
        filename = data_url.split("/")[-1].split("?")[
            0
        ]  # Adjust to strip off URL parameters
        filepath = os.path.join(self.download_dir, filename)

        if not os.path.exists(filepath):
            response = requests.get(data_url)
            if response.status_code == 200:
                with open(filepath, "wb") as f:
                    f.write(response.content)
                print(f"Downloaded {filename} to {filepath}")
            else:
                print(
                    f"Failed to download {filename}. Status code: {response.status_code}"
                )
        else:
            print(f"{filename} already exists.")

        return filepath

    def calculate_distance_by_airport(
        self, airport_name1: str, airport_name2: str
    ) -> float:
        """
        Calculates the distance between two airports given their names.

        :param airport_name1: The name of the first airport
        :param airport_name2: The name of the second airport
        :type airport_name1: str
        :type airport_name2: str
        :return: The distance in kilometers between the two airports
        :rtype: float
        """

        lat1, lon1 = self._get_airport_coordinates(airport_name1)
        lat2, lon2 = self._get_airport_coordinates(airport_name2)

        if None in (lat1, lon1, lat2, lon2):
            print(f"One or both airports not found: {airport_name1}, {airport_name2}")
            return None

        return self._calculate_distance(lat1, lon1, lat2, lon2)

    def _get_airport_coordinates(self, airport_name: str):
        """
        Retrieves the coordinates of the airport given its name.

        :param airport_name: The name of the airport
        :type airport_name: str
        :return: A tuple of latitude and longitude of the airport
        :rtype: tuple
        """
        airport_row = self.airports_df[self.airports_df["Name"] == airport_name]
        if not airport_row.empty:
            return airport_row.iloc[0]["Latitude"], airport_row.iloc[0]["Longitude"]
        return None, None

    @staticmethod
    def _calculate_distance(lat1, lon1, lat2, lon2):
        """
        Calculates the great circle distance between two points on the earth.

        :param lat1: Latitude of the first point
        :param lon1: Longitude of the first point
        :param lat2: Latitude of the second point
        :param lon2: Longitude of the second point
        :type lat1: float
        :type lon1: float
        :type lat2: float
        :type lon2: float
        :return: Distance in kilometers
        :rtype: float
        """
        lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])
        dlat = lat2 - lat1
        dlon = lon2 - lon1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        r = 6371  # Radius of Earth in kilometers
        return r * c

    def plot_airports_by_country_geo(airports_df, country_name):
        """
        Plots airports on a map for a specified country.

        :param airports_df: A dataframe containing airport information
        :param country_name: The name of the country for which to plot airports
        :type airports_df: DataFrame
        :type country_name: str
        """

        country_airports = airports_df[airports_df["Country"] == country_name]

        # Verify that there are airports for the country
        if country_airports.empty:
            print(f"No airports found for country: {country_name}")
            return

        fig, ax = plt.subplots(figsize=(10, 10))
        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
        base = world[world.name == country_name].plot(
            ax=ax, color="white", edgecolor="black"
        )
        country_airports.plot(ax=base, marker="o", color="red", markersize=5)
        plt.show()

    def plot_airport_in_country(self, country_name):
        """
        Plots all airports within a specified country.

        :param country_name: The name of the country
        :type country_name: str
        """

        country_airports = self.airports_df[self.airports_df["Country"] == country_name]

        if country_airports.empty:
            print(f"No airports found for the country: {country_name}")
            return

        gdf = gpd.GeoDataFrame(
            country_airports,
            geometry=gpd.points_from_xy(
                country_airports.Longitude, country_airports.Latitude
            ),
        )

        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))

        fig, ax = plt.subplots(1, 1, figsize=(15, 10))
        world.plot(ax=ax, color="white", edgecolor="black")

        gdf.plot(ax=ax, color="red", marker="o", label="Airports")

        plt.title(f"Airports in {country_name}")
        plt.legend()
        plt.show()

    # DAY 1 PHASE 3, 2nd point

    @staticmethod
    def haversine(lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance between two points on the Earth.
        The result is the shortest distance over the earth's surface.

        :param lon1: Longitude of the first point
        :param lat1: Latitude of the first point
        :param lon2: Longitude of the second point
        :param lat2: Latitude of the second point
        :type lon1: float
        :type lat1: float
        :type lon2: float
        :type lat2: float
        :return: The distance in kilometers
        :rtype: float
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        r = 6371  # Radius of Earth in kilometers
        return c * r

    def distance_analysis(self):
        """
        Analyzes and plots the distribution of flight distances using the haversine formula to calculate.
        This method updates the DataFrame to include distances and then visualizes a histogram of these distances.
        """

        self.routes_df["Source airport ID"] = self.routes_df[
            "Source airport ID"
        ].astype(str)
        self.routes_df["Destination airport ID"] = self.routes_df[
            "Destination airport ID"
        ].astype(str)
        self.airports_df["Airport ID"] = self.airports_df["Airport ID"].astype(str)

        routes = self.routes_df.merge(
            self.airports_df,
            how="left",
            left_on="Source airport ID",
            right_on="Airport ID",
            suffixes=("", "_src"),
        )
        routes = routes.merge(
            self.airports_df,
            how="left",
            left_on="Destination airport ID",
            right_on="Airport ID",
            suffixes=("", "_dest"),
        )

        routes["distance"] = routes.apply(
            lambda row: self.haversine(
                row["Longitude"],
                row["Latitude"],
                row["Longitude_dest"],
                row["Latitude_dest"],
            ),
            axis=1,
        )

        plt.figure(figsize=(10, 6))
        plt.hist(routes["distance"].dropna(), bins=100, color="blue", alpha=0.7)
        plt.title("Distribution of Flight Distances")
        plt.xlabel("Distance (km)")
        plt.ylabel("Number of Flights")
        plt.grid(True)
        plt.show()

    # DAY 1 phase 3, 3rd point

    def plot_flights_from_airport(self, airport_code: str, internal: bool = False):
        """
        Plots flights originating from a specific airport. If the internal flag is True, only domestic flights are
        plotted.

        :param airport_code: The IATA code of the airport to plot flights from
        :param internal: Flag to determine if only domestic flights should be plotted
        :type airport_code: str
        :type internal: bool
        """

        flights_from_airport = self.routes_df[
            self.routes_df["Source airport"] == airport_code
        ]

        if internal:
            source_country = self.airports_df.loc[
                self.airports_df["IATA"] == airport_code, "Country"
            ].values[0]
            flights_from_airport = flights_from_airport[
                flights_from_airport["Destination airport"].isin(
                    self.airports_df[self.airports_df["Country"] == source_country][
                        "IATA"
                    ].values
                )
            ]

        fig, ax = plt.subplots(figsize=(10, 10))
        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
        world.plot(ax=ax, color="white", edgecolor="black")

        for _, flight in flights_from_airport.iterrows():
            source_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Source airport"]
            ].iloc[0]
            destination_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Destination airport"]
            ].iloc[0]
            line = LineString(
                [
                    (source_airport["Longitude"], source_airport["Latitude"]),
                    (destination_airport["Longitude"], destination_airport["Latitude"]),
                ]
            )
            gpd.GeoSeries(line).plot(ax=ax, linewidth=1, color="red")

        plt.title(f'Flights from {airport_code} ({"Domestic" if internal else "All"})')
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.show()

    def plot_most_used_airplane_models(self, countries=None, top_n=20):
        """
        Plots a bar chart of the top 'n' most-used airplane models based on route data. The method can filter
        the routes by specific countries or consider all available data.
        :param countries: The countries to include in the analysis (default is None, which includes all countries).
                        Can be a list of countries or a single country as a string.
        :type countries: list, str, or None
        :param top_n: The number of top airplane models to display (default is 20).
        :type top_n: int
        """
        if isinstance(countries, str):
            countries = [countries]
        if countries is not None:
            filtered_airports = self.airports_df[
                self.airports_df["Country"].isin(countries)
            ]
            filtered_routes = self.routes_df[
                self.routes_df["Source airport ID"].isin(
                    filtered_airports["Airport ID"]
                )
                | self.routes_df["Destination airport ID"].isin(
                    filtered_airports["Airport ID"]
                )
            ]
        else:
            filtered_routes = self.routes_df
        equipment_series = filtered_routes["Equipment"].str.split().explode()
        equipment_counts = equipment_series.value_counts().head(top_n)
        top_models_names = self.airplanes_df[
            self.airplanes_df["IATA code"].isin(equipment_counts.index)
        ]
        plot_data = pd.merge(
            top_models_names,
            equipment_counts.rename("Count"),
            left_on="IATA code",
            right_index=True,
        )
        plot_data.sort_values(by="Count", ascending=False, inplace=True)
        plt.figure(figsize=(10, 6))
        plt.bar(plot_data["Name"], plot_data["Count"])
        plt.title(
            f"Top {top_n} Most Used Airplane Models"
            + (" Worldwide" if countries is None else f' in {", ".join(countries)}')
        )
        plt.xlabel("Airplane Model")
        plt.xticks(rotation=45, ha="right")
        plt.ylabel("Number of Routes")
        plt.tight_layout()
        plt.show()

    # DAY 1 PHASE 3, 5TH POINT

    def plot_flights_from_country(self, country_name: str, internal: bool = False):
        """
        Plots flights originating from a given country. If the internal parameter is True, plots only domestic flights.

        :param country_name: The name of the country from which flights originate
        :param internal: Flag to determine if only domestic flights should be plotted
        :type country_name: str
        :type internal: bool
        """
        # Filter routes originating from the given country
        source_airports = self.airports_df[self.airports_df["Country"] == country_name][
            "IATA"
        ]
        flights_from_country = self.routes_df[
            self.routes_df["Source airport"].isin(source_airports)
        ]

        # If internal is True, filter for flights where destination is also within the same country
        if internal:
            destination_airports = source_airports  # Since it's internal, destination should also be in the same
            # country
        else:
            # For international, destination can be any airport
            destination_airports = self.airports_df["IATA"]

        flights_from_country = (flights_from_country)[
            flights_from_country["Destination airport"].isin(destination_airports)
        ]

        # Plotting
        fig, ax = plt.subplots(figsize=(10, 10))
        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
        world.plot(ax=ax, color="white", edgecolor="black")

        for _, flight in flights_from_country.iterrows():
            # Look up the source and destination airport details in the airports DataFrame
            source_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Source airport"]
            ]
            destination_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Destination airport"]
            ]

            # Check if the airports exist in the airports_df DataFrame
            if not source_airport.empty and not destination_airport.empty:
                source_airport = source_airport.iloc[0]
                destination_airport = destination_airport.iloc[0]
                # Create the flight route as a line from source to destination
                line = LineString(
                    [
                        (source_airport["Longitude"], source_airport["Latitude"]),
                        (
                            destination_airport["Longitude"],
                            destination_airport["Latitude"],
                        ),
                    ]
                )
                gpd.GeoSeries(line).plot(ax=ax, linewidth=1, color="red")

        # Complete the title and labels for the plot
        flight_type = "Domestic" if internal else "International"
        plt.title(f"{flight_type} Flights from {country_name}")
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.show()
        plt.title

    def aircrafts(self):
        """
        Prints the list of aircraft models (Names) available in the airplanes dataset.
        """
        # Assuming 'Name' is the column in the airplanes_df DataFrame that contains the aircraft model names
        aircraft_models = self.airplanes_df["Name"].unique()
        print("List of aircraft models:")
        for model in aircraft_models:
            print(model)

    def aircraft_info(self, aircraft_name: str):
        """
        Fetches and prints details about the aircraft model specified by aircraft_name.
        If the aircraft_name is not found, it raises an exception with guidance for the user.

        :param aircraft_name: The name of the aircraft model to retrieve information for.
        :type aircraft_name: str
        :raises ValueError: If the aircraft_name is not in the dataset.
        :raises EnvironmentError: If the OPENAI_API_KEY is not set.
        """
        # Check if the aircraft_name is in the list of aircraft models
        if aircraft_name not in self.airplanes_df["Name"].values:
            raise ValueError(
                f"'{aircraft_name}' is not a valid aircraft model. Please use the 'aircrafts' method to list "
                f"available models."
            )

        # Retrieve the API key from environment variable
        api_key = os.getenv("OPENAI_API_KEY")
        if not api_key:
            raise EnvironmentError("OPENAI_API_KEY environment variable not set.")

        # Use the api_key with your request to the LLM API
        headers = {
            "Authorization": f"Bearer {api_key}",
            "Content-Type": "application/json",
        }
        data = {
            "model": "gpt-3.5-turbo-1106",  # Updated model name based on deprecation info
            "messages": [
                {
                    "role": "system",
                    "content": f"Give a detailed table of specifications for the aircraft model {aircraft_name} "
                    f"in Markdown format.",
                }
            ],
        }

        # Make the API call to the correct chat completions endpoint
        response = requests.post(
            "https://api.openai.com/v1/chat/completions", json=data, headers=headers
        )

        if response.status_code == 200:
            specs_table = response.json()["choices"][0]["message"][
                "content"
            ]  # Adjusted based on chat completions output format
            print(specs_table)
        else:
            print(
                "Failed to fetch aircraft information. Status code:",
                response.status_code,
            )
            print("Response body:", response.text)

    def airports(self):
        """
        Prints the list of unique IATA airport codes available in the airports dataset.
        """
        # Assuming 'IATA' is the correct column name for airport codes in your DataFrame
        airport_codes = self.airports_df["IATA"].unique()
        print("List of airport codes:")
        for code in airport_codes:
            print(code)

    def airport_info(self, airport_code: str):
        """
        Fetches and presents information about the airport specified by airport_code.
        The information includes location, services, and historical facts.

        :param airport_code: The IATA code of the airport to retrieve information for.
        :type airport_code: str
        :raises EnvironmentError: If the OPENAI_API_KEY is not set.
        """
        # Check if the airport_code exists in the DataFrame
        if airport_code not in self.airports_df["IATA"].values:
            print(f"No information found for airport code: {airport_code}")
            return

        # Retrieve the API key from environment variable
        api_key = os.getenv("OPENAI_API_KEY")
        if not api_key:
            raise EnvironmentError("OPENAI_API_KEY environment variable not set.")

        # Use the api_key with your request to the LLM API
        headers = {
            "Authorization": f"Bearer {api_key}",
            "Content-Type": "application/json",
        }
        data = {
            "model": "gpt-3.5-turbo-1106",
            "messages": [
                {
                    "role": "system",
                    "content": f"Provide a detailed description and important information about the airport with "
                    f"IATA code {airport_code}.",
                }
            ],
        }

        # Make the API call to the chat completions endpoint
        response = requests.post(
            "https://api.openai.com/v1/chat/completions", json=data, headers=headers
        )

        if response.status_code == 200:
            description = response.json()["choices"][0]["message"]["content"]
            print(description)
        else:
            print(
                "Failed to fetch airport information. Status code:",
                response.status_code,
            )
            print("Response body:", response.text)

    # Day 2 Phase 2: Descarbonization

    def plot_flights_from_country2(
        self, country_name, internal=False, short_haul_cutoff=1000
    ):
        """
        Plots flights from a specific country with an indication of whether they are short-haul or long-haul based on a
        cutoff value.

        :param country_name: The name of the country from which flights originate
        :param internal: Flag to determine if only domestic flights should be plotted
        :param short_haul_cutoff: The maximum distance in kilometers for a flight to be considered short-haul
        :type country_name: str
        :type internal: bool
        :type short_haul_cutoff: float
        """
        airports_in_country = self.airports_df[
            self.airports_df["Country"] == country_name
        ]
        flights_from_country = self.routes_df[
            self.routes_df["Source airport"].isin(airports_in_country["IATA"].values)
        ]

        if internal:
            flights_from_country = flights_from_country[
                flights_from_country["Destination airport"].isin(
                    airports_in_country["IATA"].values
                )
            ]

        fig, ax = plt.subplots(figsize=(15, 15))
        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
        world.plot(ax=ax, color="white", edgecolor="black")

        short_haul_distances = []
        seen_routes = set()

        for _, flight in flights_from_country.iterrows():
            source_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Source airport"]
            ]
            destination_airport = self.airports_df[
                self.airports_df["IATA"] == flight["Destination airport"]
            ]

            if not source_airport.empty and not destination_airport.empty:
                source_airport = source_airport.iloc[0]
                destination_airport = destination_airport.iloc[0]

                distance = self.haversine(
                    source_airport["Longitude"],
                    source_airport["Latitude"],
                    destination_airport["Longitude"],
                    destination_airport["Latitude"],
                )

                airports_pair = tuple(
                    sorted([flight["Source airport"], flight["Destination airport"]])
                )
                if distance <= short_haul_cutoff and airports_pair not in seen_routes:
                    short_haul_distances.append(distance)
                    seen_routes.add(airports_pair)

                color = "green" if distance <= short_haul_cutoff else "blue"
                line = LineString(
                    [
                        (source_airport["Longitude"], source_airport["Latitude"]),
                        (
                            destination_airport["Longitude"],
                            destination_airport["Latitude"],
                        ),
                    ]
                )
                gpd.GeoSeries(line).plot(ax=ax, linewidth=2, color=color)

        total_short_haul_distance = sum(short_haul_distances)
        num_short_haul_flights = len(seen_routes)

        # Link for https://ourworldindata.org/travel-carbon-footprint
        flight_emissions_per_km = 246  # grams CO2 per passenger km for flights
        train_emissions_per_km = 29  # grams CO2 per passenger km for electric trains
        emissions_reduction_per_km = flight_emissions_per_km - train_emissions_per_km
        total_emission_reduction = (
            total_short_haul_distance * emissions_reduction_per_km
        )
        total_emission_reduction_tonnes = total_emission_reduction / 1000000

        plt.annotate(
            f"Total short-haul flights: {num_short_haul_flights}\n"
            f"Total short-haul distance: {total_short_haul_distance:.2f} km",
            xy=(0.02, 0.98),  # Adjusted for top left position
            xycoords="axes fraction",
            fontsize=10,  # Slightly larger font
            ha="left",
            va="top",  # Align to top left
            bbox=dict(boxstyle="round,pad=0.2", fc="white", alpha=0.8),
        )  # More opaque background

        # Emission reduction annotation
        plt.annotate(
            f"Estimated emission reduction by replacing with rail: {total_emission_reduction_tonnes:.2f}"
            f" tonnes CO2",
            xy=(0.02, 0.90),  # Position just below the previous annotation
            xycoords="axes fraction",
            fontsize=10,  # Slightly larger font
            ha="left",
            va="top",  # Align to top left
            bbox=dict(boxstyle="round,pad=0.2", fc="white", alpha=0.8),
        )

        plt.title(
            f"Flights from {country_name} ({'Domestic' if internal else 'International'}) -"
            f" Short-haul cutoff: {short_haul_cutoff} km"
        )
        plt.legend(
            handles=[
                plt.Line2D(
                    [0], [0], color="green", lw=2, label="Short-haul (<1000 km)"
                ),
                plt.Line2D([0], [0], color="blue", lw=2, label="Long-haul (>1000 km)"),
            ]
        )
        plt.xlabel("Longitude")
        plt.ylabel("Latitude")
        plt.show()
