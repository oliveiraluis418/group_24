import os
import sys

sys.path.insert(0, os.path.abspath('..'))


project = 'Flight Data Analyzer'
copyright = '2024, L. Oliveira, M. Brito, G. Lourenço'
author = 'L. Oliveira, M. Brito, G. Lourenço'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc', 'sphinx_autodoc_typehints']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']