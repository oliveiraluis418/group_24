.. Flight Data Analyzer documentation master file, created by
   sphinx-quickstart on Fri Mar 15 18:23:06 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flight Data Analyzer's documentation!
================================================

.. automodule:: adpro
   :members:
   :undoc-members:
   :show-inheritance:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
